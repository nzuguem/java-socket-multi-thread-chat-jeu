package serveur;


import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

public class ServeurChat extends Thread{
	// tous ce qui est communs à tous les clients
	private int nbClient=0;
	private List<Conversation> conversations=new ArrayList<>(); 
	
	public static void main(String[] args) {
		new ServeurChat().start();
	}
	
	@Override
	public void run() {
		
		try {
			// Une fois demarrer, ce serveur peut etre tester avec le programme telnet (telnet @IpServeur port)
			ServerSocket serverSocket=new ServerSocket(23458);
			
			// attente infinie de clients
			while (true) {
				Socket socket=serverSocket.accept();
				// Ajout du client connecté à la liste
				
				++nbClient;
				// ici on demarre le Thread du client connecté
				// et execute le code de la méthode Run de Conversation
				// une fois le Thraed demarré, le serveur se remet à l'ecoute
				Conversation conversation=new Conversation(socket, nbClient);
				conversations.add(conversation);
				conversation.start();
			}
			
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void multicast(String message, String[] destiantaires){
		for(Conversation c:conversations){
			try {
				boolean trouve=false;
				for (int i = 0; i < destiantaires.length; i++) {
					if(c.getNumeroClient()==Integer.parseInt(destiantaires[i])){
						trouve=true;
						break;
					}
				}
				if(trouve==true){
					PrintWriter printWriter=new PrintWriter(c.getSocket().getOutputStream(), true);
					printWriter.println(message);
				}
				
				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	class Conversation extends Thread{
		// secket de conversation avec le client
		private Socket socket;
		// le numero unique du client sur le serveur
		private int numeroClient;
		public Conversation(Socket socket, int numeroClient) {
			super();
			this.socket = socket;
			this.numeroClient=numeroClient;
		}


		@Override
		public void run() {
			try {
				
				// reception du client
				InputStream inputStream=socket.getInputStream();
				InputStreamReader inputStreamReader=new InputStreamReader(inputStream);
				BufferedReader bufferedReader=new BufferedReader(inputStreamReader);
			
			
				// Envoi au client
				OutputStream outputStream=socket.getOutputStream();
				PrintWriter printWriter=new PrintWriter(outputStream,true);
				
				// recuperer l'adresse IP de client
				String ip=socket.getRemoteSocketAddress().toString();
				
				System.out.println("Connexion du client numero "+numeroClient);
				System.out.println("IP = "+ip);
				printWriter.println("Bienvenue vous etes le client numero "+numeroClient);
				
				// Pour maintenir le client connecté jusqu'à sa deconnexion
				while (true) {
					// Reception d'une requete
					String request;
					while((request=bufferedReader.readLine())!=null){
						// message-1,2,3
						String[] messageRecu=request.split("-");
						String message=messageRecu[0];
						String[] destinataires=messageRecu[1].split(",");
						multicast(message,destinataires);	
					}
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}


		/**
		 * @return the socket
		 */
		public Socket getSocket() {
			return socket;
		}


		/**
		 * @param socket the socket to set
		 */
		public void setSocket(Socket socket) {
			this.socket = socket;
		}


		/**
		 * @return the numeroClient
		 */
		public int getNumeroClient() {
			return numeroClient;
		}


		/**
		 * @param numeroClient the numeroClient to set
		 */
		public void setNumeroClient(int numeroClient) {
			this.numeroClient = numeroClient;
		}
		
		
	}
}
