package serveur;


import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

public class ServeurMT extends Thread{
	// tous ce qui est communs à tous les Threads clients
	// Il constitue pour nous des sections critiques car accessible en meme temps par nos Threads 
	// En lecture il n y a pas de problème
	// Mais en ecriture il se pose un problème
	// Donc faut les protégé par un MUTEX (synchronized en JAVA)
	private int nbClient=0;
	private int nombreSecret;
	private boolean fin=false;
	private String gagnant;
	
	private static ServeurMT serveurMT;
	
	
	public static void main(String[] args) {
		serveurMT=new ServeurMT();
		serveurMT.start();
	}
	
	@Override
	public void run() {
		
		try {
			// Une fois demarrer, ce serveur peut etre tester avec le programme telnet (telnet @IpServeur port)
			ServerSocket serverSocket=new ServerSocket(23458);
			// generation du nombre secret
			nombreSecret=(int)(Math.random()*1000);
			// attente infinie de clients
			while (true) {
				Socket socket=serverSocket.accept();
				++nbClient;
				// ici on demarre le Thread du client connecté
				// et execute le code de la méthode Run de Conversation
				// une fois le Thraed demarré, le serveur se remet à l'ecoute
				new Conversation(socket, nbClient).start();
			}
			
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	class Conversation extends Thread{
		// secket de conversation avec le client
		private Socket socket;
		// le numero unique du client sur le serveur
		private int numeroClient;
		public Conversation(Socket socket, int numeroClient) {
			super();
			this.socket = socket;
			this.numeroClient=numeroClient;
		}


		@Override
		public void run() {
			try {
				
				// reception du client
				InputStream inputStream=socket.getInputStream();
				InputStreamReader inputStreamReader=new InputStreamReader(inputStream);
				BufferedReader bufferedReader=new BufferedReader(inputStreamReader);
			
			
				// Envoi au client
				OutputStream outputStream=socket.getOutputStream();
				PrintWriter printWriter=new PrintWriter(outputStream,true);
				
				// recuperer l'adresse IP de client
				String ip=socket.getRemoteSocketAddress().toString();
				
				System.out.println("Connexion du client numero "+numeroClient);
				System.out.println("IP = "+ip);
				printWriter.println("Bienvenue vous etes le client numero "+numeroClient);
				if(fin==false){
					printWriter.println(" Devinez le nombre secret entre 0 et 1000");	
				}
				// Pour maintenir le client connecté jusqu'à sa deconnexion
				while (true) {
					// Reception d'une requete
					String request;
					while((request=bufferedReader.readLine())!=null){
						System.out.println(ip+" a envoyé "+request);
						int nbEnvoye = 0;
						try {
							nbEnvoye = Integer.parseInt(request);
						} catch (Exception e) {
							System.out.println(ip+" n'a pas envoyé un entier :"+ request);
							printWriter.println(" Veuillez entrer un entier ! MERCI");
						}
						
						// On teste si le jeu n'est pas encore terminé
						if(fin==false){
							if(nbEnvoye<nombreSecret){
								printWriter.println(" Votre nombre est plus petit");
							}else if (nbEnvoye>nombreSecret){
								printWriter.println(" Votre nombre est plus grand");
							}else{
								// Zone critique
								// "serveurMT" car c'est dans cette objet qu'il y a les variables critiques(fin, gagnant, ...)
								// A la modification de ces variables, "serveurMT" est verrouillé
								// Donc tous Threads qui voudra accéder s'endormira jusqu'à ce que le Thread en section critique termine
								synchronized (serveurMT) {
									gagnant=ip;
									fin=true;
								}
								
								printWriter.println("Bavo vous avez gagné");
								System.out.println("**********************");
								System.out.println("BRAVO Mr "+ip);
								System.out.println("**********************");
							}
						}else{
							printWriter.println(" Le jeu est terminé et le gagnant est "+gagnant);
							
						}
					}
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
	}
}
